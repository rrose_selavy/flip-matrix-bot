use chrono_humanize::{Accuracy, HumanTime, Tense};
use matrix_sdk::room::{Joined, RoomMember};
use matrix_sdk::ruma::OwnedEventId;

use super::super::sender::{Message, MessageText};
use super::super::EArgs;
use crate::context::EventHelper;
use crate::data::FlipEventContent;
use crate::website::Website;

fn list_fmt(event: &FlipEventContent, long: bool, website: &Website, host: &RoomMember) -> String {
    let (id, time, place, server_info, suffix) = if long {
        let id = event.id_hex();
        let time = event.time_tz().to_rfc3339();
        let place = format!(" in {}", event.meeting_place,);

        let server_info = if let Some(server) = &event.game_server {
            let password_info = if let Some(password) = &event.game_server_pwd {
                format!(" Password: {password}")
            } else {
                String::new()
            };
            format!("  \nServer: `{server}`{password_info}")
        } else {
            String::new()
        };

        let suffix = format!(
            "    Scheduled in [{}]({}) on [{}]({}).\n\n",
            event.mx_room,
            event.mx_room_permalink(),
            event.mx_time_tz(),
            event.mx_msg_permalink(),
        );
        (id, time, place, server_info, suffix)
    } else {
        let id = format!("{:.5}", event.id_hex());
        let time = event
            .time_tz()
            .format("%a, %b %-d %-I:%M%P UTC%:z")
            .to_string();
        let empty = String::new();
        let suffix = "\n".to_string();
        (id, time, empty.clone(), empty, suffix)
    };

    let name = if let Some(url) = website.url_string_with_segment(&event.id_hex()) {
        format!("[**{}**]({})", event.name_or_game(), url)
    } else {
        format!("**{}**", event.name_or_game())
    };

    let (tense, hs) = if event.passed {
        (Tense::Past, "")
    } else {
        (Tense::Future, " from now")
    };

    let ht = HumanTime::from(event.time_tz()).to_text_en(Accuracy::Rough, tense) + hs;

    let cancelled = if event.cancelled {
        "(Cancelled) - Would have been"
    } else {
        "-"
    };

    let ownership_asterisk = if event.ownership_required { "" } else { "\\*" };

    let display_name = host.display_name().unwrap();

    format!(
        "`{}` - {} {} {}  \n    Game: {}{} - Hosted by [{}]({}) on {}{}.{}  \n{}",
        id,
        name,
        cancelled,
        ht,
        event.md_game(),
        ownership_asterisk,
        display_name,
        event.host.matrix_to_uri(),
        time,
        place,
        server_info,
        suffix,
    )
}

pub async fn upcoming(args: EArgs, joined_room: Joined, state: EventHelper) -> MessageText {
    let Some(mut events) = state.store.get_events(Some(10)).await else {
        return "No events found.".into()
    };

    let mut count: u8 = 0;
    let mut m = String::new();
    let mut ownership_asterisk_included = false;
    events.sort_by_key(|event| event.time);
    for e in events {
        if e.passed || e.cancelled {
            continue;
        };

        count += 1;
        if !e.ownership_required {
            ownership_asterisk_included = true;
        };
        let host = joined_room.get_member(&e.host).await.unwrap().unwrap();
        let msg = list_fmt(&e, args.long, &state.website, &host);
        m = format!("{m}- {msg}");
    }
    m = match count {
        0 => "No upcoming events found. Try scheduling a new one with `!f events add`.".to_string(),
        1 => format!("One upcoming event:\n\n{m}"),
        _ => format!("Upcoming events:\n\n{m}"),
    };

    if let Some(feed_url) = state.calendar.url {
        m = format!("{m}\n\nSubscribe to the calendar feed: [{feed_url}]({feed_url})  \n");
    };

    if ownership_asterisk_included {
        m = format!("{m}\\* ownership of game is not required to participate in event.");
    };

    m.into()
}

pub async fn by_id(
    id: String,
    args: EArgs,
    joined_room: Joined,
    state: EventHelper,
) -> MessageText {
    let Some(events) = state.store.get_event_by_id(&id).await else {
        return format!("No event with ID `{id}` found.").into();
    };

    let mut ownership_asterisk_included = false;
    let mut m = if events.len() == 1 {
        if !events[0].ownership_required {
            ownership_asterisk_included = true;
        };
        let host = joined_room
            .get_member(&events[0].host)
            .await
            .unwrap()
            .unwrap();
        format!(
            "Event found:\n\n- {}",
            list_fmt(&events[0], args.long, &state.website, &host)
        )
    } else {
        let mut msg =
            format!("Disambiguation needed: multiple events found matching ID `{id}`:\n\n");
        for e in events {
            if !e.ownership_required {
                ownership_asterisk_included = true;
            };
            let host = joined_room.get_member(&e.host).await.unwrap().unwrap();
            let li = list_fmt(&e, true, &state.website.clone(), &host);
            msg = format!("{msg}- {li}");
        }
        msg
    };

    if ownership_asterisk_included {
        m = format!("{m}\n\n\\* ownership of game is not required to participate in event.");
    };

    m.into()
}

pub async fn by_mx_id(
    mx_id: OwnedEventId,
    args: EArgs,
    joined_room: Joined,
    state: EventHelper,
) -> MessageText {
    let Some(event) = state.store.get_event_by_mx_id(mx_id.clone()).await else {
        return format!("No Matrix message with ID `{mx_id}` found.").into();
    };

    let host = joined_room.get_member(&event.host).await.unwrap().unwrap();
    let mut m = format!(
        "Event found:\n\n- {}",
        list_fmt(&event, args.long, &state.website, &host)
    );

    if !event.ownership_required {
        m = format!("{m}\n\n\\* ownership of game is not required to participate in event.");
    };

    m.into()
}

pub async fn run(args: EArgs, joined_room: Joined, state: EventHelper) -> Message {
    let (oid, omx_id) = (args.id.clone(), args.mx_id.clone());
    match (oid, omx_id) {
        (Some(id), _) => by_id(id, args, joined_room, state).await,
        (_, Some(mx_id)) => by_mx_id(mx_id, args, joined_room, state).await,
        _ => upcoming(args, joined_room, state).await,
    }
    .into()
}
