use anyhow::anyhow;
use chrono::{Duration, Offset};
use chrono_humanize::{Accuracy, HumanTime, Tense};
use matrix_sdk::room::Joined;
use matrix_sdk::ruma::{
    events::{room::message::RoomMessageEventContent, OriginalSyncMessageLikeEvent},
    OwnedEventId,
};

use super::super::sender::{Message, MessageText};
use super::super::{CanEvents, ModEvents};
use crate::context::EventHelper;
use crate::data::{parse_time, FlipEventContent};

async fn get_event(
    oid: Option<String>,
    omx_id: Option<OwnedEventId>,
    state: &EventHelper,
) -> Result<FlipEventContent, MessageText> {
    let event = match (oid, omx_id) {
        (Some(id), _) => {
            let opt = state.store.get_event_by_id(&id).await;
            let Some(mut events) = opt else {
                return Err(format!("No event with ID `{id}` found.").into());
            };
            if events.len() > 1 {
                return Err(format!("Disambiguation needed: multiple events found matching ID `{id}`.  \nTry running `!f events {id}` to get the full ID.").into());
            }
            events.pop().unwrap()
        }
        (_, Some(mx_id)) => {
            let Some(event) = state.store.get_event_by_mx_id(mx_id.clone()).await else {
                return Err(format!("No Matrix message with ID `{mx_id}` found.").into());
            };
            event
        }
        _ => unreachable!(),
    };
    Ok(event)
}

pub async fn cancel(
    args: CanEvents,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) -> Message {
    let oevent = get_event(args.id.clone(), args.mx_id.clone(), &state).await;
    let mut event = match oevent {
        Ok(v) => v,
        Err(e) => return e.into(),
    };

    let requester = joined_room.get_member(&ev.sender).await.unwrap().unwrap();
    let moderator = requester.normalized_power_level() >= 50;
    let scheduler = event.host == ev.sender;

    if moderator || scheduler {
        if event.cancelled {
            let msg = format!("Event `{}` has already been cancelled.", event.id_hex());
            return MessageText::new(msg).into();
        };
        if event.passed {
            let msg = format!("Event `{}` has already taken place.", event.id_hex());
            return MessageText::new(msg).into();
        };
        let id = event.id;
        event.generation += 1;
        event.cancelled = true;
        let msg = format!("Event `{}` cancelled.", event.id_hex());

        state
            .store
            .update_event_by_full_id(event.clone(), id)
            .await
            .unwrap();
        state.reminders.delete(&id).await;
        state.calendar.add_event(event.clone(), true).await;
        state.website.add_event(event).await;

        MessageText::new(msg).into()
    } else {
        anyhow!(
            "You must be the event host or a room moderator to cancel event `{}`",
            event.id_hex()
        )
        .into()
    }
}

pub async fn run(
    args: ModEvents,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) -> Message {
    let oevent = get_event(args.id.clone(), args.mx_id.clone(), &state).await;
    let mut event = match oevent {
        Ok(v) => v,
        Err(e) => return e.into(),
    };

    if event.host == ev.sender {
        if event.passed {
            return MessageText::new(format!(
                "Event `{}` has already taken place.",
                event.id_hex()
            ))
            .into();
        };
        if event.cancelled && !args.uncancel {
            return MessageText::new(format!(
                "Event `{}` has been cancelled. Consider adding `--uncancel` to revert this.",
                event.id_hex()
            ))
            .into();
        };
        if args.server_password.is_some() && !(args.server.is_some() || event.game_server.is_some())
        {
            return anyhow!("error: A server must be specified in order to set a server password. Try adding '--server <server_address>'").into();
        }

        let mut changed = false;
        let mut tz_diff = None;
        let mut ownership_note = "";

        if args.name.is_some() && args.name != event.name {
            changed = true;
            event.name = args.name;
        };
        if let Some(game) = args.game {
            if game != event.game {
                changed = true;
                event.game = game;
            };
        };
        if let Some(game_url) = args.game_url {
            if game_url != event.game_url {
                changed = true;
                event.game_url = game_url;
            };
        };
        if let Some(timezone) = args.timezone {
            if timezone != event.timezone {
                changed = true;

                let time = event.time;
                let old_offset_s = time
                    .with_timezone(&event.timezone)
                    .offset()
                    .fix()
                    .local_minus_utc();
                let offset_s = time
                    .with_timezone(&timezone)
                    .offset()
                    .fix()
                    .local_minus_utc();
                let diff = offset_s - old_offset_s;
                if diff != 0 {
                    tz_diff = Some(diff);
                };

                event.timezone = timezone;
            };
        };
        if let Some(meeting_place) = args.meeting_place {
            if meeting_place != event.meeting_place {
                changed = true;
                event.meeting_place = meeting_place;
            };
        };
        if args.server.is_some() && args.server != event.game_server {
            changed = true;
            event.game_server = args.server;
        };
        if args.server_password.is_some() && args.server_password != event.game_server_pwd {
            changed = true;
            event.game_server_pwd = args.server_password;
        };
        if args.ownership_required {
            if !event.ownership_required {
                ownership_note = " *(ownership required to attend)*";
                changed = true;
                event.ownership_required = true;
            };
        } else if args.ownership_not_required && event.ownership_required {
            changed = true;
            event.ownership_required = false;
        };
        if args.uncancel && event.cancelled {
            changed = true;
            event.cancelled = false;
        };

        match args.time {
            Some(time_string) => {
                let times = parse_time(&time_string, event.timezone, ev.origin_server_ts);

                let time = match times {
                    Ok((t, _)) => t,
                    Err(e) => return e.into(),
                };

                if time != event.time {
                    changed = true;
                    event.time = time;
                }
            }
            None => {
                if let Some(diff) = tz_diff {
                    let diff_duration = Duration::seconds(diff.into());
                    event.time -= diff_duration;
                }
            }
        };

        if changed {
            event.generation += 1;
        } else {
            return MessageText::new(format!("Event `{}` has not been changed, as none of the provided values were different from the current version of the event.", event.id_hex())).into();
        };

        let id = event.id;
        state
            .store
            .update_event_by_full_id(event.clone(), id)
            .await
            .unwrap();
        state.reminders.delete(&id).await;
        state
            .reminders
            .schedule(event.clone(), joined_room.clone())
            .await;
        state.calendar.add_event(event.clone(), true).await;
        state.website.add_event(event.clone()).await;

        let webpage = match state.website.url_string_with_segment(&event.id_hex()) {
            Some(url) => url,
            None => String::new(),
        };

        if !event.ownership_required {
            ownership_note = " (ownership not required to attend)";
        };

        let ht = HumanTime::from(event.time_tz()).to_text_en(Accuracy::Rough, Tense::Future);
        let name = match event.name {
            Some(ref n) => format!("**Name:** {n}  \n"),
            None => String::new(),
        };

        let server_info = if let Some(server) = &event.game_server {
            let password_info = if let Some(password) = &event.game_server_pwd {
                format!("**Password:** {password}")
            } else {
                String::new()
            };
            format!("  \n**Server:** `{server}`  \n{password_info}")
        } else {
            String::new()
        };

        let msg = format!(
            "### Event modified:\n\n\
            {}**Game:** {}{}  \n\
            **Time:** {} ({}, [see local time]({}))  \n\
            **Meeting place:** {}{}  \n\
            **ID:** `{}`",
            name,
            event.md_game(),
            ownership_note,
            event.time_tz().to_rfc2822(),
            ht,
            webpage,
            event.meeting_place,
            server_info,
            event.id_hex(),
        );
        MessageText::new(msg).into()
    } else {
        anyhow!(
            "You must be the event host to modify event `{}`",
            event.id_hex()
        )
        .into()
    }
}
