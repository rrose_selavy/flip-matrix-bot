// Add events subcommand

use chrono_humanize::{Accuracy, HumanTime, Tense};
use matrix_sdk::room::{Joined, RoomMember};
use matrix_sdk::ruma::events::{
    room::message::RoomMessageEventContent, OriginalSyncMessageLikeEvent,
};

use super::super::{
    sender::{Message, MessageText},
    AddEvents,
};
use crate::context::EventHelper;
use crate::data::FlipEventContent as Event;
use crate::website::Website;

fn creation_success(event: &Event, website: &Website, host: &RoomMember) -> MessageText {
    let webpage = match website.url_string_with_segment(&event.id_hex()) {
        Some(url) => url,
        None => String::new(),
    };

    let name = match event.name {
        Some(ref n) => format!("**Name:** {n}  \n"),
        None => String::new(),
    };

    let ownership_note = if event.ownership_required {
        ""
    } else {
        " (ownership not required to attend)"
    };

    let ht = HumanTime::from(event.time_tz()).to_text_en(Accuracy::Rough, Tense::Future);

    let server_info = if let Some(server) = &event.game_server {
        let password_info = if let Some(password) = &event.game_server_pwd {
            format!("**Password:** {password}")
        } else {
            String::new()
        };
        format!("  \n**Server:** `{server}`  \n{password_info}")
    } else {
        String::new()
    };

    // The member is within an Option, which is with a Result. Hence, two unwraps.
    let display_name = host.display_name().unwrap();
    let extra_thx = match host.normalized_power_level() {
        0..=49 => ", and contributing to what makes FLiP special!",
        _ => "!",
    };

    let msg = format!(
        "### Event scheduled:\n\n\
        {}**Game:** {}{}  \n\
        **Time:** {} ({}, [see local time]({}))  \n\
        **Meeting place:** {}{}\n\n\
        Thanks, [{}]({}), for [volunteering to host]({}){} \
        Use `!f events modify {}` to change these values.",
        name,
        event.md_game(),
        ownership_note,
        event.time_tz().to_rfc2822(),
        ht,
        webpage,
        event.meeting_place,
        server_info,
        display_name,
        event.host.matrix_to_uri(),
        event.mx_msg_permalink(),
        extra_thx,
        event.id_hex(),
    );
    msg.into()
}

pub async fn run(
    args: AddEvents,
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    joined_room: Joined,
    state: EventHelper,
) -> Message {
    let event = Event::new(
        args,
        &ev,
        joined_room.room_id().to_owned(),
        joined_room.route().await.unwrap(),
    );

    match event {
        Ok(v) => {
            state.clone().add(v.clone(), joined_room.clone()).await;

            let host = joined_room.get_member(&v.host).await.unwrap().unwrap();
            creation_success(&v, &state.website, &host).into()
        }
        Err(e) => e.into(),
    }
}
