use url::Url;

use super::sender::{Message, MessageText};
use super::IArgs;
use super::Icoms;
use crate::context::EventHelper;

fn general() -> MessageText {
    "[Friendly Linux Players (FLiP)](https://friendlylinuxplayers.org/) is an inclusive online gaming community of Linux enthusiasts. \
    If you have interest in the community, and can stick to our [Code of Conduct](https://friendlylinuxplayers.org/conduct), you are welcome here! Try `!f events` to see upcoming gaming events.  \n\
    **Matrix space**: [FLiP Community](https://matrix.to/#/#community:flip.earth)  \n\
    **Mumble server**: `mumble.flip.earth`  \n\
    **Steam group**: [Friendly Linux Players](https://steamcommunity.com/groups/FriendlyLinuxPlayers)".into()
}

fn website() -> MessageText {
    "https://friendlylinuxplayers.org".into()
}

fn conduct() -> MessageText {
    "https://friendlylinuxplayers.org/conduct".into()
}

fn space() -> MessageText {
    "Join the community space to better discover and organize all of our official rooms: \
    [FLiP Community](https://matrix.to/#/#community:flip.earth)"
        .into()
}

fn events(cal_url: Option<Url>) -> MessageText {
    let mut m = "Anyone in the community can volunteer to host a gaming event. To list or create \
                 events, use the `events` subcommand. Try `!f help events` to see how to use this."
        .to_string();
    if let Some(feed_url) = cal_url {
        m = format!("{m}\n\nSubscribe to the calendar feed: [{feed_url}]({feed_url})");
    };

    m.into()
}

fn mumble() -> MessageText {
    "Join our [Mumble](https://www.mumble.info/) server for voice chat: \
    `mumble://mumble.flip.earth`  \n\
    Note that registering yourself allows you to create temporary channels."
        .into()
}

fn steam() -> MessageText {
    "Join the [Steam group](https://steamcommunity.com/groups/FriendlyLinuxPlayers). \
    Note that joining is strictly optional for participating in the community."
        .into()
}

fn source() -> MessageText {
    "https://gitlab.com/FriendlyLinuxPlayers/flip-matrix-bot".into()
}

fn gitlab() -> MessageText {
    "https://gitlab.com/FriendlyLinuxPlayers".into()
}

fn bot_threads() -> MessageText {
    "If you are using a Matrix client which supports threads, and the bot \
    replies in a thread, please send further responses within the thread, so as \
    to reduce clutter in the room when crafting your command."
        .into()
}

fn guests() -> MessageText {
    "The numbered users in this room are guest users. These are dynamically \
    created when someone tries to access the room without being signed in to a \
    Matrix account, lowering the barrier to participate.\n\n\
    Eventually, these may be removed automatically."
        .into()
}

pub fn run(args: &IArgs, state: EventHelper) -> Message {
    match args.icommands {
        None => general(),
        Some(Icoms::Website) => website(),
        Some(Icoms::Conduct) => conduct(),
        Some(Icoms::Space) => space(),
        Some(Icoms::Events) => events(state.calendar.url),
        Some(Icoms::Mumble) => mumble(),
        Some(Icoms::Steam) => steam(),
        Some(Icoms::Source) => source(),
        Some(Icoms::Gitlab) => gitlab(),
        Some(Icoms::BotThreads) => bot_threads(),
        Some(Icoms::Guests) => guests(),
    }
    .into()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn events_with_calendar() {
        let response = MessageText::new(
            "Anyone in the community can volunteer to host a gaming event. To list \
            or create events, use the `events` subcommand. Try `!f help events` to see how to use \
            this.\n\nSubscribe to the calendar feed: \
            [webcal://flip.earth/feed/events.ics](webcal://flip.earth/feed/events.ics)"
                .to_string(),
        );
        let url = Url::parse("webcal://flip.earth/feed/events.ics").unwrap();
        assert_eq!(response, events(Some(url)));
    }

    #[test]
    fn events_without_calendar() {
        let response = MessageText::new(
            "Anyone in the community can volunteer to host a gaming event. To list \
            or create events, use the `events` subcommand. Try `!f help events` to see how to use \
            this."
                .to_string(),
        );
        assert_eq!(response, events(None));
    }
}
