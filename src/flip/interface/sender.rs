use anyhow::Error;
use clap::{builder::Command, error::Error as ClapError};
use matrix_sdk::room::Joined;
use matrix_sdk::ruma::events::{
    room::message::{ForwardThread, ReplyWithinThread, RoomMessageEventContent},
    OriginalSyncMessageLikeEvent,
};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct MessageText {
    pub text: String,
    markdown: bool,
}

impl MessageText {
    pub fn new(text: String) -> MessageText {
        MessageText {
            text,
            markdown: true,
        }
    }

    pub fn markdown(mut self, enabled: bool) -> MessageText {
        self.markdown = enabled;
        self
    }
}

impl From<String> for MessageText {
    fn from(item: String) -> MessageText {
        MessageText::new(item)
    }
}

impl From<&str> for MessageText {
    fn from(item: &str) -> MessageText {
        MessageText::new(item.to_string())
    }
}

#[derive(Debug)]
pub enum Message {
    Unthreaded(MessageText),
    Reply(MessageText),
    Threaded(MessageText),
    Error(Error),
}

impl From<MessageText> for Message {
    fn from(item: MessageText) -> Message {
        Message::Reply(item)
    }
}

impl From<Error> for Message {
    fn from(item: Error) -> Message {
        Message::Error(item)
    }
}

pub async fn send(
    message: Message,
    joined_room: Joined,
    ev: Option<OriginalSyncMessageLikeEvent<RoomMessageEventContent>>,
    cmd: Option<&mut Command>,
) {
    match message {
        Message::Unthreaded(x) => {
            let content = notice(x.text.clone(), x.markdown);
            joined_room.send(content, None).await.unwrap();
        }
        Message::Reply(y) => {
            let full_ev = &ev
                .unwrap()
                .into_full_event(joined_room.room_id().to_owned());
            let content =
                notice(y.text.clone(), y.markdown).make_reply_to(full_ev, ForwardThread::No);
            joined_room.send(content, None).await.unwrap();
        }
        Message::Threaded(z) => {
            let full_ev = &ev
                .unwrap()
                .into_full_event(joined_room.room_id().to_owned());
            let content =
                notice(z.text.clone(), z.markdown).make_for_thread(full_ev, ReplyWithinThread::No);
            joined_room.send(content, None).await.unwrap();
        }
        Message::Error(e) => {
            let msg = match e.downcast::<ClapError>() {
                Ok(v) => cmd.unwrap().error(v.kind(), v).to_string(),
                Err(e) => e.to_string(),
            };
            let full_ev = &ev
                .unwrap()
                .into_full_event(joined_room.room_id().to_owned());
            let content = notice(msg, false).make_for_thread(full_ev, ReplyWithinThread::No);
            joined_room.send(content, None).await.unwrap();
        }
    };
}

fn notice(msg: String, markdown: bool) -> RoomMessageEventContent {
    if markdown {
        RoomMessageEventContent::notice_markdown(msg)
    } else {
        RoomMessageEventContent::notice_plain(msg)
    }
}
