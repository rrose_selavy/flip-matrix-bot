// Parse chat messages

use anyhow::{anyhow, Error};
use chrono::Local;
use chrono_tz::Tz;
use clap::{
    error::{Error as ClapError, ErrorKind},
    ArgGroup, Args, CommandFactory, Parser, Subcommand,
};
use interim::{parse_date_string, Dialect};
use matrix_sdk::room::Joined;
use matrix_sdk::ruma::{
    events::{room::message::RoomMessageEventContent, OriginalSyncMessageLikeEvent},
    OwnedEventId,
};
use shell_words;
use url::Url;

use crate::context::EventHelper;

mod events;
mod information;
pub mod sender;

#[derive(Parser)]
#[command(author = "@HER0:matrix.org", version, about, long_about = None)]
struct Chat {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Schedule, list, and manage community gaming events
    ///
    /// With no subcommand, lists upcoming events.
    Events(EArgs),
    /// Display information on the FLiP community
    ///
    /// Use subcommands to get details about a specific topic.
    Information(IArgs),
}

#[derive(Args)]
#[command(visible_alias = "e", args_conflicts_with_subcommands = true)]
#[command(group(
            ArgGroup::new("by")
                .args(["id", "mx_id"]),
        ))]
pub struct EArgs {
    /// Display events in long format
    #[arg(short, long)]
    long: bool,
    /// Display events matching an ID
    #[arg(short, long)]
    id: Option<String>,
    /// Display event created by the message with a given Matrix event ID
    #[arg(short, long)]
    mx_id: Option<OwnedEventId>,
    #[command(subcommand)]
    ecommands: Option<Ecoms>,
}

#[derive(Subcommand)]
enum Ecoms {
    /// Schedule a new event
    Add(AddEvents),
    /// Modify an upcoming event
    Modify(ModEvents),
    /// Cancel an upcoming event
    Cancel(CanEvents),
}

#[derive(Args)]
pub struct AddEvents {
    /// The title of this event (e.g. "Virtual board game night")
    #[arg(short, long)]
    pub name: Option<String>,
    /// The game to be played
    pub game: String,
    /// Link to the game (for example, the Steam store page)
    pub game_url: Url,
    /// Time that the event starts (e.g. "2017-06-30 15:40" or "Friday 6pm")
    #[arg(value_parser = is_fuzzy_time)]
    pub time: String,
    /// Timezone of the scheduled time (e.g. "US/Central", "CET", or "Etc/GMT+4")
    #[arg(default_value = "UTC")]
    pub timezone: Tz,
    /// Where everyone can talk during the event (like a Mumble channel or Matrix room)
    #[arg(
        short,
        long,
        default_value = "the Event channel on `mumble.flip.earth`"
    )]
    pub meeting_place: String,
    /// Ownership of the game is not required to attend this event
    #[arg(short = 'O', long)]
    pub ownership_not_required: bool,
    /// The server that the game will be played on
    #[arg(short, long)]
    pub server: Option<String>,
    /// The password required to connect to the game server
    #[arg(short = 'p', long, requires = "server")]
    pub server_password: Option<String>,
}

#[derive(Args)]
#[command(visible_alias = "mod")]
#[command(group(
            ArgGroup::new("by")
                .required(true)
                .args(["id", "mx_id"]),
        ))]
#[command(group(
            ArgGroup::new("ownership")
                .args(["ownership_required", "ownership_not_required"]),
        ))]
#[command(group(
            ArgGroup::new("field")
                .required(true)
                .args(["name", "game", "game_url", "time", "timezone", "meeting_place", "ownership_required", "ownership_not_required", "server", "server_password", "uncancel"])
                .multiple(true),
        ))]
pub struct ModEvents {
    /// Modify by event ID, as shown when listing events
    id: Option<String>,
    /// Modify by the Matrix event ID of the command which created the event
    #[arg(short, long)]
    mx_id: Option<OwnedEventId>,
    /// Set the title of the event
    #[arg(short, long)]
    name: Option<String>,
    /// Set the game to be played
    #[arg(short, long)]
    game: Option<String>,
    /// Set the game URL
    #[arg(short = 'u', long)]
    game_url: Option<Url>,
    /// Set the event start time (e.g. "2017-06-30 15:40" or "Friday 6pm")
    #[arg(short, long, value_parser = is_fuzzy_time)]
    time: Option<String>,
    /// Set the timezone for the event start time (e.g. "US/Central", "CET", or "Etc/GMT+4")
    ///
    /// Note that changing this to a timezone with a different offset without changing the time
    /// will change the start time of the event.
    #[arg(short = 'T', long)]
    timezone: Option<Tz>,
    /// Set where everyone can talk during the event
    #[arg(short = 'M', long)]
    meeting_place: Option<String>,
    /// Ownership of the game is required to attend this event
    #[arg(short = 'o', long)]
    pub ownership_required: bool,
    /// Ownership of the game is not required to attend this event
    #[arg(short = 'O', long)]
    pub ownership_not_required: bool,
    /// The server that the game will be played on
    #[arg(short, long)]
    pub server: Option<String>,
    /// The password required to connect to the game server
    #[arg(short = 'p', long)]
    pub server_password: Option<String>,
    /// Revert the cancelling of a cancelled event.
    #[arg(short = 'C', long)]
    uncancel: bool,
}

#[derive(Args)]
#[command(visible_alias = "can")]
#[command(group(
            ArgGroup::new("by")
                .required(true)
                .args(["id", "mx_id"]),
        ))]
pub struct CanEvents {
    /// Cancel by event ID, as shown when listing events
    id: Option<String>,
    /// Cancel by the Matrix event ID of the command which created the event
    #[arg(short, long)]
    mx_id: Option<OwnedEventId>,
}

#[derive(Args)]
#[command(visible_alias = "i", visible_alias = "info")]
pub struct IArgs {
    #[command(subcommand)]
    icommands: Option<Icoms>,
}

#[derive(Subcommand)]
enum Icoms {
    /// Get a link to the website
    Website,
    /// Get a link to the Code of Conduct
    Conduct,
    /// Get information on our Matrix space
    Space,
    /// Get information on community events
    #[command(visible_alias = "calendar")]
    Events,
    /// Get information about our Mumble server
    #[command(visible_alias = "voice")]
    Mumble,
    /// Get information about our Steam group
    Steam,
    /// Get a link to the flip-matrix-bot source code
    Source,
    /// Get a link to our GitLab organization
    Gitlab,
    /// Get information on using threads with the bot
    BotThreads,
    /// Get information on guests in the community
    Guests,
}

fn is_fuzzy_time(s: &str) -> Result<String, Error> {
    let _ = parse_date_string(s, Local::now(), Dialect::Us)?;
    Ok(s.to_owned())
}

pub async fn process(
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    command: String,
    joined_room: Joined,
    state: EventHelper,
) {
    let mut cmd = Chat::command().bin_name("!f");
    let Ok(param_itr) = shell_words::split(&command) else {
        let err: ClapError = ClapError::raw(ErrorKind::Format, "Unable to split command into parameters. Did you miss a quote?");
        let response: sender::Message = anyhow!(err).into();
        sender::send(response, joined_room, Some(ev), Some(&mut cmd)).await;
        return;
    };

    let chat = Chat::try_parse_from(param_itr);

    let response = match chat {
        Err(err) => anyhow!(err).into(),
        Ok(args) => match args.command {
            Commands::Events(e) => events::run(e, ev.clone(), joined_room.clone(), state).await,
            Commands::Information(ref i) => information::run(i, state),
        },
    };

    sender::send(response, joined_room, Some(ev), Some(&mut cmd)).await;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn time_fuzzy() {
        let fuzzy = "Monday 6am";
        let validated = is_fuzzy_time(fuzzy).unwrap();
        assert_eq!(validated, fuzzy.to_string());
    }
    #[test]
    fn time_clear() {
        let clear = "2552-07-31 18:56";
        let validated = is_fuzzy_time(clear).unwrap();
        assert_eq!(validated, clear.to_string());
    }
}
