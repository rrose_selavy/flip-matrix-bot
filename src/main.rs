// Parse CLI arguments and initiate Matrix connection

use anyhow::Error;
use clap::Parser;
use matrix_sdk::ruma::{OwnedRoomId, OwnedRoomOrAliasId};
use matrix_sdk::{config::SyncSettings, Client};
use std::path::PathBuf;
use url::Url;

mod calendar;
mod context;
mod data;
mod flip;
mod reminders;
mod storage;
mod website;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// URL of bot's homeserver
    #[arg(short = 'H', long, default_value = "https://matrix.flip.earth")]
    homeserver: Url,
    /// Bot's username (e.g. for "@botty:flip.earth" it would be "botty")
    #[arg(short, long)]
    username: String,
    /// Bot's password
    #[arg(short, long)]
    password: String,
    /// Room alias or ID to join (e.g. "#room:flip.earth")
    #[arg(short, long)]
    room: OwnedRoomOrAliasId,
    /// Room ID for storing data (e.g. "!<ID>:flip.earth")
    #[arg(short, long)]
    storage_room: OwnedRoomId,
    /// Display name for the bot to use
    #[arg(short, long)]
    display_name: Option<String>,
    /// Where to write the iCalendar file
    #[arg(short, long, default_value = "./events.ics")]
    calendar_path: PathBuf,
    /// URL where the iCalendar file can be accessed (e.g. "webcal://flip.earth/feed/events.ics")
    #[arg(short = 'C', long)]
    calendar_url: Option<Url>,
    /// Directory to write event data for the website (e.g.
    /// "/var/www/flip-grav/user/pages/02.events/flip-matrix-bot")
    #[arg(short, long, default_value = "./")]
    website_path: PathBuf,
    /// Where the event pages on the website are accessible (e.g.
    /// "https://friendlylinuxplayers.org/events/")
    #[arg(short = 'W', long)]
    website_url: Option<Url>,
}

async fn connect(
    homeserver_url: Url,
    username: String,
    password: String,
    room: OwnedRoomOrAliasId,
    storage_room: OwnedRoomId,
    display_name: Option<String>,
    calendar_path: PathBuf,
    calendar_url: Option<Url>,
    website_path: PathBuf,
    website_url: Option<Url>,
) -> Result<(), Error> {
    let user_agent = format!("{}/{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
    let client_builder = Client::builder()
        .homeserver_url(homeserver_url)
        .user_agent(&user_agent)
        .handle_refresh_tokens();

    let client = client_builder.build().await?;

    client
        .login_username(&username, &password)
        .initial_device_display_name("{user_agent}")
        .send()
        .await?;
    println!("Logged in");

    if display_name.is_some() {
        client
            .account()
            .set_display_name(display_name.as_deref())
            .await?;
    };

    // Initial sync. Doing this first avoids the bot responding to old messages.
    // It also allows us to refresh the room state that we are checking below.
    let initial_response = client.sync_once(SyncSettings::default()).await?;

    let joined_storage = client.join_room_by_id(&storage_room).await?;
    println!("Joined storage room: {storage_room}");

    let joined = client
        .join_room_by_id_or_alias(&room, &[room.server_name().to_owned()])
        .await?;
    let joined_id = joined.room_id().to_owned();
    println!("Joined room: {joined_id}");

    let store = storage::Store::new(joined_storage);
    let reminders = reminders::Events::default();
    let calendar = calendar::Calendar::new(calendar_path, calendar_url);
    let website = website::Website::new(website_path, website_url);

    let reminder_store = store.clone();
    let refresh_reminders = reminders.clone();
    tokio::spawn(async move {
        refresh_reminders
            .schedule_from_store(&reminder_store, joined)
            .await;
    });

    let calendar_store = store.clone();
    let populate_calendar = calendar.clone();
    tokio::spawn(async move {
        populate_calendar.populate_events(&calendar_store).await;
    });

    let website_store = store.clone();
    let populate_website = website.clone();
    tokio::spawn(async move {
        populate_website.populate_events(&website_store).await;
    });

    let helper = context::EventHelper {
        store,
        reminders,
        calendar,
        website,
    };
    client.add_event_handler_context(helper);

    client.add_room_event_handler(&joined_id, flip::event_handler);

    let settings = SyncSettings::default().token(initial_response.next_batch);
    println!("Beginning sync loop");
    client.sync(settings).await?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Cli::parse();

    connect(
        args.homeserver,
        args.username,
        args.password,
        args.room,
        args.storage_room,
        args.display_name,
        args.calendar_path,
        args.calendar_url,
        args.website_path,
        args.website_url,
    )
    .await?;
    Ok(())
}
