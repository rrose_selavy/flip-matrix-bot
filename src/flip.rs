// Determine whether incoming messages are ones we have interest in

use matrix_sdk::ruma::{
    api::client::receipt::create_receipt::v3::ReceiptType,
    events::{
        receipt::ReceiptThread,
        room::message::{MessageType, Relation, RoomMessageEventContent},
        OriginalSyncMessageLikeEvent,
    },
};
use matrix_sdk::{event_handler::Ctx, room::Room};

use crate::context::EventHelper;

pub mod interface;

pub async fn event_handler(
    ev: OriginalSyncMessageLikeEvent<RoomMessageEventContent>,
    room: Room,
    state: Ctx<EventHelper>,
) {
    let Room::Joined(joined) = room else {
        let room_id = room.room_id();
        panic!("Message received from room {room_id}, but membership in room has been revoked.");
    };

    let MessageType::Text(ref text_content) = ev.content.msgtype else {
        return;
    };

    let thread_root = if let Some(Relation::Thread(ref thread)) = ev.content.relates_to {
        ReceiptThread::Thread(thread.event_id.clone())
    } else {
        ReceiptThread::Main
    };

    joined
        .send_single_receipt(ReceiptType::Read, thread_root, ev.event_id.clone())
        .await
        .unwrap();

    let body = &text_content.body;
    if body.starts_with("!f ") {
        let owned = body.clone();
        let inner = state.0;
        interface::process(ev, owned, joined, inner).await;
    }
}
