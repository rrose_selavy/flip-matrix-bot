use std::{fs, path::PathBuf};
use tokio::{fs::File, io::AsyncWriteExt};
use url::Url;

use crate::data::FlipEventContent;
use crate::storage::Store;

#[derive(Clone, Debug)]
pub struct Website {
    pub url: Option<Url>,
    file_path: PathBuf,
}

fn sanitize_yaml(single_quoted_value: &str) -> String {
    single_quoted_value.replace('\'', "''")
}

fn sanitize_path(game_name: &str) -> String {
    let chars = game_name.chars();
    let mut sanitized_name = String::new();

    for c in chars {
        if c.is_ascii_alphanumeric() || c == '-' {
            sanitized_name.push(c);
        } else if c == ' ' {
            sanitized_name += "_";
        }
    }

    sanitized_name
}

fn url_add_segment(url: &Url, path_segment: &str) -> Url {
    let mut new_url = url.clone();

    new_url
        .path_segments_mut()
        .unwrap()
        .pop_if_empty()
        .push(path_segment);

    new_url
}

impl Website {
    pub fn new(file_path: PathBuf, url: Option<Url>) -> Website {
        Website { url, file_path }
    }

    pub async fn add_event(&self, event: FlipEventContent) {
        let date = format!("{}", event.time.date_naive().format("%F"));
        let title = format!("Event: {} - {}", event.name_or_game(), date);

        let (url_str, default_path, canonical_path) = if self.url.is_some() {
            let default = self
                .url_with_segments(vec![
                    date.as_str(),
                    sanitize_path(&event.game).as_str(),
                    &event.id_hex()[0..4],
                ])
                .unwrap();
            let canonical_url = self.url_with_segments(Some(event.id_hex())).unwrap();
            (
                String::from(canonical_url.clone()),
                String::from(default.path()),
                String::from(canonical_url.path()),
            )
        } else {
            (String::new(), String::new(), String::new())
        };

        let routes = format!(
            "routes:\n  \
              default: '{default_path}'\n  \
              canonical: '{canonical_path}'"
        );

        let description = format!(
            "Come play {} with the FLiP community on {}.",
            sanitize_yaml(&event.game),
            date
        );
        let metadata = format!(
            "metadata:\n  \
              description: '{}'\n  \
              keywords: 'Linux, Gaming, Event, Community'\n  \
              color-scheme: 'dark'\n  \
              'og:title': '{}'\n  \
              'og:type': 'website'\n  \
              'og:url': '{}'\n  \
              'og:description': '{}'\n  \
              'og:site_name': 'Friendly Linux Players'",
            &description,
            sanitize_yaml(&title),
            url_str,
            &description,
        );

        // Grav uses YAML-formatted data in the header of Markdown files.
        let frontmatter = format!(
            "---\n\
            title: '{}'\n\
            date: {}\n\
            {}\n\
            {}\n\
            cache_control: no-cache\n\n\
            name_or_game: '{}'\n\
            id_hex: '{}'\n\
            mx_room_permalink: '{}'\n\
            mx_msg_permalink: '{}'\n\n\
            {}\
            ---",
            sanitize_yaml(&title),
            event.time.to_rfc3339(),
            routes,
            metadata,
            sanitize_yaml(&event.name_or_game()),
            event.id_hex(),
            event.mx_room_permalink(),
            event.mx_msg_permalink(),
            serde_yaml::to_string(&event).unwrap(),
        );

        let directory = self.file_path.join(event.id_hex());
        fs::create_dir_all(&directory).unwrap();
        let path = directory.join("flip-event.md");
        let mut file = File::create(path).await.unwrap();
        file.write_all_buf(&mut frontmatter.as_bytes())
            .await
            .unwrap();
    }

    pub async fn populate_events(&self, store: &Store) {
        let option = store.get_events(None).await;
        if let Some(events) = option {
            for e in events {
                self.add_event(e).await;
            }
        }
    }

    fn url_with_segments(
        &self,
        path_segments: impl IntoIterator<Item = impl AsRef<str>>,
    ) -> Option<Url> {
        match self.url.clone() {
            Some(mut url) => {
                for segment in path_segments {
                    url = url_add_segment(&url, segment.as_ref());
                }
                Some(url)
            }
            None => None,
        }
    }

    pub fn url_string_with_segment(&self, path_segment: &str) -> Option<String> {
        self.url_with_segments(Some(path_segment))
            .map(std::convert::Into::into)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn escape_single_quotes() {
        let unescaped = "Let's test";
        let sanitized = sanitize_yaml(unescaped);
        assert_eq!(sanitized, "Let''s test");
    }
    #[test]
    fn sanitize_game_name() {
        let name = "G$a&m?e N/a:m_e-1";
        let sanitized = sanitize_path(name);
        assert_eq!(sanitized, "Game_Name-1".to_string());
    }
    #[test]
    fn add_segment_to_url() {
        let url = Url::parse("https://example.com").unwrap();
        let new_url = url_add_segment(&url, "segment");
        assert_eq!(new_url.as_ref(), "https://example.com/segment");
    }
    #[test]
    fn add_segment_to_url_trailing_slash() {
        let url = Url::parse("https://example.com/").unwrap();
        let new_url = url_add_segment(&url, "segment");
        assert_eq!(new_url.as_ref(), "https://example.com/segment");
    }
}
