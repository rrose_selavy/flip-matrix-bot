// Store and access data from a Matrix room

use anyhow::Error;
use matrix_sdk::room::{Joined, MessagesOptions};
use matrix_sdk::ruma::{
    api::client::filter::{LazyLoadOptions, RoomEventFilter},
    events::{relation::Replacement, room::message::Relation},
    OwnedEventId,
};
use std::collections::HashMap;

use crate::data::{FlipEventContent, OriginalFlipEvent};

#[derive(Clone, Debug)]
pub struct Store {
    room: Joined,
}

fn make_uniq(vec: &mut Vec<OriginalFlipEvent>) {
    let mut uniq_ids: HashMap<u64, u8> = HashMap::new();
    let events: Vec<OriginalFlipEvent> = vec
        .clone()
        .into_iter()
        .filter(|e| {
            let c = e.content.clone();
            match uniq_ids.get(&c.id) {
                Some(stored_gen) if stored_gen >= &c.generation => false,
                _ => {
                    uniq_ids.insert(c.id, c.generation);
                    true
                }
            }
        })
        .collect();
    *vec = events;
}

impl Store {
    pub fn new(room: Joined) -> Store {
        Store { room }
    }

    pub async fn store_event(&self, event: FlipEventContent) {
        self.room.send(event, None).await.unwrap();
    }

    pub async fn get_events(&self, approx_count: Option<usize>) -> Option<Vec<FlipEventContent>> {
        self.passed_update().await;

        let (mut events, mut end) = self.get_mx_events(None).await;
        match approx_count {
            Some(n) => {
                while events.len() < n {
                    let mut v = self.get_mx_events(end).await;
                    events.append(&mut v.0);
                    make_uniq(&mut events);

                    end = v.1;
                    if end.is_none() {
                        break;
                    }
                }
            }
            None => {
                while end.is_some() {
                    let mut v = self.get_mx_events(end).await;
                    events.append(&mut v.0);
                    make_uniq(&mut events);
                    end = v.1;
                }
            }
        }

        let flip_events: Vec<FlipEventContent> = events.into_iter().map(|e| e.content).collect();

        if flip_events.is_empty() {
            return None;
        };

        Some(flip_events)
    }

    pub async fn get_event_by_id(&self, id: &str) -> Option<Vec<FlipEventContent>> {
        self.passed_update().await;

        let (mut events, mut end) = self.get_mx_events(None).await;
        while end.is_some() {
            let mut v = self.get_mx_events(end).await;
            events.append(&mut v.0);
            end = v.1;
        }

        make_uniq(&mut events);

        let matches: Vec<FlipEventContent> = events
            .into_iter()
            .filter_map(|e| {
                let c = e.content;
                if c.id_hex().starts_with(id) {
                    Some(c)
                } else {
                    None
                }
            })
            .collect();

        if matches.is_empty() {
            None
        } else {
            Some(matches)
        }
    }

    pub async fn get_event_by_mx_id(&self, mx_id: OwnedEventId) -> Option<FlipEventContent> {
        let (mut events, mut end) = self.get_mx_events(None).await;
        while end.is_some() {
            let mut v = self.get_mx_events(end).await;
            events.append(&mut v.0);
            end = v.1;
        }

        make_uniq(&mut events);

        events.into_iter().find_map(|e| {
            let c = e.content;
            if c.mx_msg == mx_id {
                Some(c)
            } else {
                None
            }
        })
    }

    pub async fn update_event_by_full_id(
        &self,
        content: FlipEventContent,
        id: u64,
    ) -> Result<(), Error> {
        let (mut events, mut end) = self.get_mx_events(None).await;
        while end.is_some() {
            let mut v = self.get_mx_events(end).await;
            events.append(&mut v.0);
            end = v.1;
        }

        make_uniq(&mut events);

        let original_mx_id = events
            .into_iter()
            .find_map(|e| {
                if e.content.id == id {
                    Some(e.event_id)
                } else {
                    None
                }
            })
            .unwrap();

        self.update_event_by_store_mx_msg(content, original_mx_id)
            .await
    }

    async fn passed_update(&self) {
        let (mut events, mut end) = self.get_mx_events(None).await;
        while end.is_some() {
            let mut v = self.get_mx_events(end).await;
            events.append(&mut v.0);
            end = v.1;
        }

        make_uniq(&mut events);

        for e in events {
            let mut c = e.content;
            if !c.passed && c.is_in_past() {
                c.generation += 1;
                c.passed = true;
                self.update_event_by_store_mx_msg(c, e.event_id)
                    .await
                    .unwrap();
            }
        }
    }

    async fn update_event_by_store_mx_msg(
        &self,
        content: FlipEventContent,
        id: OwnedEventId,
    ) -> Result<(), Error> {
        let inner = Box::new(content.clone());
        let mut c = content;
        let r = Relation::Replacement(Replacement::new(id, inner));
        c.relates_to = Some(r);
        self.room.send(c, None).await?;

        Ok(())
    }

    async fn get_mx_events(
        &self,
        from: Option<String>,
    ) -> (Vec<OriginalFlipEvent>, Option<String>) {
        let mut opts = MessagesOptions::backward();
        opts.from = from;

        let mut filter = RoomEventFilter::empty();
        filter.senders = Some(vec![self.room.own_user_id().to_owned()]);
        filter.types = Some(vec!["earth.flip.event".to_string()]);
        filter.lazy_load_options = LazyLoadOptions::Enabled {
            include_redundant_members: true,
        };
        opts.filter = filter;

        let msgs = self.room.messages(opts).await.unwrap();
        let events = msgs.chunk;

        let mut uniq_ids: HashMap<u64, u8> = HashMap::new();
        let flip_events = events
            .into_iter()
            .filter_map(|f| {
                let Ok(e) = f.event.deserialize_as::<OriginalFlipEvent>() else {
                    return None;
                };
                let gen = e.content.generation;
                let id = e.content.id;
                match uniq_ids.get(&id) {
                    Some(stored_gen) if stored_gen >= &gen => None,
                    _ => {
                        uniq_ids.insert(id, gen);
                        Some(e)
                    }
                }
            })
            .collect();
        (flip_events, msgs.end)
    }
}
