use chrono::{Duration, Utc};
use chrono_humanize::HumanTime;
use matrix_sdk::room::Joined;
use std::{collections::HashMap, sync::Arc};
use tokio::{sync::Mutex, task::JoinSet, time::sleep};

use crate::data::FlipEventContent;
use crate::flip::interface::sender::{send, Message, MessageText};
use crate::storage::Store;

#[derive(Clone, Debug, Default)]
pub struct Events {
    tasks: Arc<Mutex<HashMap<u64, JoinSet<()>>>>,
}

fn event_fmt(event: &FlipEventContent) -> String {
    let name = match &event.name {
        Some(n) => format!("**Event:** {n}  \n"),
        None => String::new(),
    };

    let ownership_note = if event.ownership_required {
        ""
    } else {
        " (ownership not required to attend)"
    };

    let server_info = if let Some(server) = &event.game_server {
        let password_info = if let Some(password) = &event.game_server_pwd {
            format!("**Password:** {password}")
        } else {
            String::new()
        };
        format!("  \n**Server:** `{server}`  \n{password_info}")
    } else {
        String::new()
    };

    format!(
        "{name}**Game:** {}{}  \n\
        **Host:** [{}]({})  \n\
        **Meeting place:** {}{}",
        event.md_game(),
        ownership_note,
        event.host,
        event.host.matrix_to_uri(),
        event.meeting_place,
        server_info,
    )
}

fn set_reminder(reminder: MessageText, when: Duration, room: Joined, set: &mut JoinSet<()>) {
    let timer = async move {
        sleep(when.to_std().unwrap()).await;
        send(Message::Unthreaded(reminder), room, None, None).await;
    };
    set.spawn(timer);
}

impl Events {
    pub async fn schedule(&self, event: FlipEventContent, room: Joined) {
        if event.cancelled || event.passed {
            return;
        };
        let mut set = JoinSet::new();

        let id = event.id;
        let time = event.time - Utc::now();

        let hour = Duration::hours(1);
        let short_reminder = if time > hour * 2 {
            Some(time - hour)
        } else if time > Duration::minutes(19) {
            Some(time / 2)
        } else {
            None
        };

        let day_reminder = if time > Duration::hours(30) {
            Some(time - Duration::days(1))
        } else {
            None
        };

        let msg = event_fmt(&event);
        let happening = format!("### Event starting now\n\n{msg}");
        set_reminder(happening.into(), time, room.clone(), &mut set);

        if let Some(reminder_time) = short_reminder {
            let ht = HumanTime::from(time - reminder_time);
            let reminder = format!("### Event starting {ht}\n\n{msg}");
            set_reminder(reminder.into(), reminder_time, room.clone(), &mut set);
        }

        if let Some(reminder_time) = day_reminder {
            let reminder = format!(
                "Event starting in 24 hours:  \n\
                **{}** - Game: {} - Hosted by [{}]({})",
                event.name_or_game(),
                event.md_game(),
                event.host,
                event.host.matrix_to_uri(),
            );
            set_reminder(reminder.into(), reminder_time, room, &mut set);
        };

        self.tasks.lock().await.insert(id, set);
    }

    pub async fn delete(&self, id: &u64) {
        self.tasks.lock().await.remove(id);
    }

    pub async fn schedule_from_store(&self, store: &Store, room: Joined) {
        let opt = store.get_events(None).await;
        let Some(events) = opt else {
            return;
        };

        for e in events {
            self.schedule(e, room.clone()).await;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::{from_value, json};

    fn dummy_event() -> FlipEventContent {
        let event_json = json!({
          "id": "3092327158754853044",
          "generation": 1,
          "name": "Event name",
          "game": "Game",
          "game_url": "https://example.net/",
          "game_server": null,
          "ownership_required": true,
          "time": "3023-02-04T22:00:00Z",
          "timezone": "UTC",
          "meeting_place": "Middle of nowhere",
          "host": "@user:matrix.org",
          "mx_time": "2023-01-28T01:07:13.938Z",
          "mx_room": "!somewhere:example.org",
          "mx_msg": "$1231231231231230IWLui:matrix.org",
          "mx_route": [],
          "cohosts": null,
          "attendees": null,
          "stream": null,
          "passed": false,
          "cancelled": false
        });
        from_value(event_json).unwrap()
    }

    #[test]
    fn event_formatting() {
        let event = dummy_event();
        assert_eq!(
            event_fmt(&event),
            "**Event:** Event name  \n\
            **Game:** [Game](https://example.net/)  \n\
            **Host:** [@user:matrix.org](https://matrix.to/#/@user:matrix.org)  \n\
            **Meeting place:** Middle of nowhere"
        );
    }

    #[test]
    fn event_formatting_no_name() {
        let mut event = dummy_event();
        event.name = Default::default();
        assert_eq!(
            event_fmt(&event),
            "**Game:** [Game](https://example.net/)  \n\
            **Host:** [@user:matrix.org](https://matrix.to/#/@user:matrix.org)  \n\
            **Meeting place:** Middle of nowhere"
        );
    }
}
